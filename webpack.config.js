const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = (env, argv) => {
  var config = {
    mode: 'production',
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'bundle.js',
    } ,
    devServer: {
      client: {
        overlay: {
          errors: true,
          warnings: false,
        },
      },
      static: {
        directory: path.join(__dirname, 'public'),
        publicPath: '',
      },
      compress: true,
      port: 9000,
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.csv/,
          type: 'asset/source',
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
        },
      ],
    },
    optimization: {
      minimize: true,
      minimizer: [
          new TerserPlugin(),
      ],
    }
  };
  if (argv.mode === 'development') {
    config.optimization.minimize = false;
    config.mode = 'development';
  }
  return config;
};