# haptic-floor-ui

Interactive haptic floor visualizations for monitoring displays and previewing immersive experiences for the Haptic Floor by the Society for Arts and Technology [SAT].

## Authors

* [Valentine Auphan](http://valentine.auphan.ensad.fr/): design
* [Claire Paillon](https://ca.linkedin.com/in/claire-paillon-04777812): design
* [Christian Frisson](https://frisson.re): design and development

## Installation

### Install requirements
```
sudo apt install npm sed
```
### Install dependencies with node package manager
```
npm i
```

### Generate with webpack

2 options:
1. if you want to develop with auto-reload:
```
npm run dev
```
2. if you want to generate a bundle:
```
npm run build
```
